import pytest

@pytest.fixture(autouse=True)
def starter():
    print("\nHere we start")
    yield
    print("\nHere we stop")
