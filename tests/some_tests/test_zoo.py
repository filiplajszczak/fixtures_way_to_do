import pytest
from our_great_code.zoo import animal_detector


def test_animal_detector(swallows, important_numbers):
    for animal in swallows:
        assert animal_detector(animal)

    for number in important_numbers:
        assert not animal_detector(number)