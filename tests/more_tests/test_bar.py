from our_great_code.bar import square


def test_square(important_numbers):
    for number in important_numbers:
        assert square(number) == number**2
