from our_great_code.foo import returner


def test_returner_returns_swallows(swallows):
    for swallow in swallows:
        assert returner(swallow) == swallow


def test_returner_returns_numbers(important_numbers):
    for number in important_numbers:
        assert returner(number) == number
